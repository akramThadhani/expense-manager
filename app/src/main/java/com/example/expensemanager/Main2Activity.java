package com.example.expensemanager;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    //key to send user input data back to Main activity
    public static final String INPUT_DOUBLE = "com.example.expensemanager.MESSAGE";

    ImageView btmImage;
    //on button click function to send data back to Main activity to calculate the final expense
    public void onClickFunction (View view)
    {
        Button addBtn = findViewById(R.id.addBtn);

        //collect user input and pass it to Main activity Result function
        EditText userInput = findViewById(R.id.rentInput);
        String userInputString = userInput.getText().toString();
        Log.i("User input double:  ", userInputString);
        //create new intent to send the data back
        Intent newIntent = new Intent();
        //Using putExtra to insert the key with the user input data
        newIntent.putExtra(INPUT_DOUBLE, userInputString);
        //using set result to send to the onActivityResult function in Main activity with result code
        setResult(RESULT_OK, newIntent);

        Toast.makeText(this, "Input Value Entered: " + userInput.getText().toString(), Toast.LENGTH_LONG).show();
        //indicating this activity is complete and this will close the current activity and open the previous activity on the stack
        finish();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btmImage = (ImageView) findViewById(R.id.imageView2);


        //create new intent to receive Intent from Main activity and display the text to the layout
        Intent incomingIntent = getIntent();
        String displayTopMsg = incomingIntent.getStringExtra(MainActivity.TOPTEXT_MESSAGE);
        TextView displayMsg = findViewById(R.id.displayMsg);
        displayMsg.setText(displayTopMsg);
        //setting text for currency in the layout
        TextView displayCurrency = findViewById(R.id.currencyAud);
        displayCurrency.setText("AUD");
        //Creating a bundle in order to get image sent in intent from main Activity seperately
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            //using the ID sent from the previous activity to extract the image
            int resId = bundle.getInt("resID");
            btmImage.setImageResource(resId);
        }



    }
}
