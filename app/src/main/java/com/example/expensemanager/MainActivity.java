package com.example.expensemanager;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView myListView;
    public static final String TOPTEXT_MESSAGE = "com.example.expensemanager.MESSAGE";
    public static double totalExpense = 0;
    //overriding the onActivityResult class in order to extract the result from the other activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //using super class to perform all other functions of the class
        super.onActivityResult(requestCode, resultCode, data);
        //checking if the request code and result code match the correct intent which was initially sent for each user option (Like rent, travel etc) and the intent used to send back the data with result code
        if (requestCode == 1 && resultCode == RESULT_OK)
        {
            //extracting the data from the key with the data which was sent from the Main2Activity
            String inputActivity2 = data.getStringExtra(Main2Activity.INPUT_DOUBLE);

            Log.i("HomeRent Info", "Home Rent:  " + inputActivity2);
            //checking if data received is valid or null
            if (inputActivity2 != null)
            {   //extracting the value of the string type data and storing as double
                double rentExp = Double.valueOf(inputActivity2);
                //data extracted to be added on to the total expense variable for calculation of final result
                totalExpense = totalExpense + rentExp;
                Log.i("Home rent Info: ", "rentExpense: " + rentExp);
                Log.i("Home rent Info: ", " Total Expense: " + totalExpense);
                //setting the layout to display the total expenses as each expense is selected by the user
                TextView total = (TextView) findViewById(R.id.resultDisplay);
                String tot = String.valueOf(totalExpense);
                total.setText("Total Spend: $" + tot);
            }

        }
        if (requestCode == 2 && resultCode == RESULT_OK)
        {
            String inputActivity2 = data.getStringExtra(Main2Activity.INPUT_DOUBLE);

            Log.i("Eating info", "Eating: " + inputActivity2);

            if (inputActivity2 != null)
            {
                double eatingExp = Double.valueOf(inputActivity2);

                totalExpense = totalExpense + eatingExp;
                Log.i("Eating info: ", "EatingExpense: " + eatingExp);
                Log.i("Eating info: ", " Total Expense: " + totalExpense);

                TextView total = (TextView) findViewById(R.id.resultDisplay);
                String tot = String.valueOf(totalExpense);
                total.setText("Total Spend: $" + tot);

            }
        }
        if (requestCode == 3 && resultCode == RESULT_OK)
        {
            String inputActivity2 = data.getStringExtra(Main2Activity.INPUT_DOUBLE);

            Log.i("Travel info", "We are here right now " + inputActivity2);

            if (inputActivity2 != null)
            {
                double travelExp = Double.valueOf(inputActivity2);

                totalExpense = totalExpense + travelExp;
                Log.i("Travel info: ", "Travel Expense: " + travelExp);
                Log.i("Travel info: ", " Total Expense: " + totalExpense);

                TextView total = (TextView) findViewById(R.id.resultDisplay);
                String tot = String.valueOf(totalExpense);
                total.setText("Total Spend: $" + tot);
            }
        }
        if (requestCode == 4 && resultCode == RESULT_OK)
        {
            String inputActivity2 = data.getStringExtra(Main2Activity.INPUT_DOUBLE);

            Log.i("Shopping Info", "We are here right now " + inputActivity2);

            if (inputActivity2 != null)
            {
                double shoppingExp = Double.valueOf(inputActivity2);

                totalExpense = totalExpense + shoppingExp;
                Log.i("Shopping Info: ", "shopping Expense: " + shoppingExp);
                Log.i("Shopping Info: ", " Total Expense: " + totalExpense);

                TextView total = (TextView) findViewById(R.id.resultDisplay);
                String tot = String.valueOf(totalExpense);
                total.setText("Total Spend: $" + tot);


            }
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //assign listView object
        myListView = (ListView) findViewById(R.id.listViewID);
        //create an array of string type for the list of options
        final ArrayList <String> myArrayList = new ArrayList<>();
        //add in the names of the strings
        myArrayList.add("Home Rent");
        myArrayList.add("Eating Out");
        myArrayList.add("Travel");
        myArrayList.add("Shopping");
        //create an Array adapter to adapt the array list created above onto the layout
        final ArrayAdapter myArrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,myArrayList);
        //set the adapter to the list view object
        myListView.setAdapter(myArrayAdapter);
        //onItem click function to open an Activity if any item from the list view object is clicked
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Log.i("info", "Array Position: " + position);
                String listPos = myArrayAdapter.getItem(position).toString();
                //if item clicked = to the content of the list item then open new activity
                if (listPos.equals("Home Rent"))
                {
                    String rentMsg = "Enter your Home Rent Expense: ";
                    //creating a new intent to pass the message to the next activity
                    Intent homeRentIntent = new Intent(MainActivity.this, Main2Activity.class);
                    homeRentIntent.putExtra(TOPTEXT_MESSAGE, rentMsg );
                    //using this intent to also send an image to the next activity with a seperate object ID
                    homeRentIntent.putExtra("resID", R.drawable.house);
                    //using this function to receive back data from the intent sent out
                    startActivityForResult(homeRentIntent, 1);

                }
                if (listPos.equals("Eating Out"))
                {
                    String eatingOutMsg = "Enter your Eating Out Expense: ";
                    Intent eatingOut = new Intent(MainActivity.this, Main2Activity.class);
                    eatingOut.putExtra(TOPTEXT_MESSAGE, eatingOutMsg );
                    eatingOut.putExtra("resID", R.drawable.food);
                    startActivityForResult(eatingOut, 2);

                }
                if (listPos.equals("Travel"))
                {
                    String travelMsg = "Enter your Travel Expense: ";
                    Intent travel = new Intent(MainActivity.this, Main2Activity.class);
                    travel.putExtra(TOPTEXT_MESSAGE, travelMsg );
                    travel.putExtra("resID", R.drawable.travel);
                    startActivityForResult(travel, 3);

                }
                if (listPos.equals("Shopping"))
                {
                    String shoppingMsg = "Enter your Shopping Expense: ";
                    Intent shopping = new Intent(MainActivity.this, Main2Activity.class);
                    shopping.putExtra(TOPTEXT_MESSAGE, shoppingMsg );
                    shopping.putExtra("resID", R.drawable.shopping);
                    startActivityForResult(shopping, 4);

                }
            }
        });

    }

}
